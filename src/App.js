import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route } from 'react-router-dom';
import Prismic from 'prismic-javascript';
import rootReducer from './vb_data_layer/reducers';
import configureStore from './store/configureStore';
// import linkResolver from './prismic-configuration.js'
import IndexForm from './components/indexForm';
import FlightForm from './components/flightForm';
import SeatsForm from './components/seatsForm';

// import logo from './logo.svg';
import './App.css';

// import { Date, Link, RichText } from 'prismic-reactjs'
const apiEndpoint = 'https://vivaair.cdn.prismic.io/api/v2';
const accessToken =
  'MC5XNlRiOFNjQUFDY0ExaGlq.77-977-9bO-_vQgs77-977-9Ou-_ve-_ve-_ve-_ve-_ve-_ve-_ve-_ve-_ve-_vRwwQnbvv71fSiTvv73vv73vv71iHg';
const Client = Prismic.client(apiEndpoint, { accessToken });

const store = configureStore(rootReducer, {});

function App() {
  const [doc, setDocData] = React.useState(null);
  React.useEffect(() => {
    const fetchData = async () => {
      const response = await Client.query(
        Prismic.Predicates.fulltext('document', '')
      );
      if (response) {
        setDocData(response);
      }
    };
    fetchData();
  }, []);
  console.log('Prismic Docs', doc);

  return (
    <div className="App">
      <Provider store={store}>
        <BrowserRouter>
          {/* <div className="App-logo">
            <img src="/img/vb-logo.png" alt="Viva Aerobus Logo"></img>
          </div> */}
          <div className="App-body">
            <Route path="/" component={IndexForm} exact />
            <Route path="/vuelos" component={FlightForm} />
            <Route path="/asientos" component={SeatsForm} />
            {/*  <Route component={Error} /> */}
          </div>
        </BrowserRouter>
      </Provider>
    </div>
  );
}

export default App;
