import React, { Component } from 'react';
import { Redirect } from "react-router-dom";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import get from 'lodash/get';
import Loader from 'react-loader-spinner';
import * as tripDataActions from '../vb_data_layer/actions/tripData';

import createTripDoc from '../docs/createTrip';

class SeatsForm extends Component {

    state = {
        showLoader: true,
        getApiData: true,
        createTrip: false
    }
    
    async componentDidMount() {
        let createTrip = "";

        if( this.state.getApiData === false ){
            createTrip = createTripDoc;
        
        } else {
            
            if( this.props.location.state === undefined ){
                createTrip = "noProps";

            } else {
                this.props.actions.getTripData(this.props.location.state.createTripData);
            }
        }

        this.setState({ showLoader: false });
    }
    
    render() {

        const { createTripArray } = this.props;

        return (
            this.state.showLoader === true
            ? <Loader type="Puff" color="#FFFFFF" height={100} width={100} />
            : createTripArray === "noProps"
            ? <Redirect to='/'/>
            :
            <div>
                <h4>CREATE TRIP REQUEST</h4>
                <pre>{JSON.stringify(this.props.location.state.createTripData, null, 1)}</pre>

                <h4>CREATE TRIP RESPONSE</h4>
                <pre>{JSON.stringify(createTripArray, null, 1)}</pre>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
      createTripArray: get(state, 'tripData.createTripArray', [])
    };
  };
  
  const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({ ...tripDataActions }, dispatch)
  });
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(SeatsForm);

