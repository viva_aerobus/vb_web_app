import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import get from 'lodash/get';
import { Collapse } from 'react-collapse';
import moment from 'moment';
import Loader from 'react-loader-spinner';
import searchResponseDoc from '../docs/searchResponse';
import * as flightsDataActions from '../vb_data_layer/actions/flightsData';


class FlightForm extends Component {

    state = {
        showLoader: true,
        getApiData: true,
        searchResponse: false,
        flightsInfo: [],
        isOpenedId: false,
    }

    async componentDidMount() {

        let searchResponse = "";
        let flightsInfo = "";

        if( this.state.getApiData === false ){
            searchResponse = searchResponseDoc;
            flightsInfo = searchResponseDoc.routes[0].flights;

        } else {

            if( this.props.location.state === undefined ){
                searchResponse = "noProps";

            } else {
                await this.props.actions.getFlightsData(this.props.location.state.searchFlights);
            }
        }

        this.setState({ searchResponse, flightsInfo, showLoader: false });
    }

    toggleChange(value) {
        this.setState({ tripType: value });
    }

    collapseFlightResult(id){
        this.setState({ isOpenedId: id });
    }

    createTripData(journeyKey, fareAvailabilityKey, price){
        const tripData = {
            "journeys":[
                {
                    "journeyKey": journeyKey,
                    "fareAvailabilityKey": fareAvailabilityKey,
                    "price": price
                }],
            "passengers":[]
            }

        this.props.location.state.searchFlights.passengers.forEach(info => {
            for (var i = 0; i < info.count; i++) tripData.passengers.push({"type": info.code});
        })

        return tripData
    }

    render() {
        const { searchResponse } = this.props;
        const flightsInfo = get(searchResponse, 'flights', []);
        return (
        <div>{
            this.state.showLoader === true
            ? <Loader type="Puff" color="#FFFFFF" height={100} width={100} />
            : searchResponse === "noProps"
            ? <Redirect to='/'/>

            : searchResponse === "noFlights" || flightsInfo.length === 0
            ? <div className="noFlights">
                <h1>Sin vuelos disponibles</h1>
                <Link to="/">
                <button type="button" className="btn btn-light btn-sm btn-block">
                    Regresar
                </button>
                </Link>
            </div>
            :
            <div>
            <nav><ul className="pagination justify-content-center flights-bar">
                <li className="page-item">
                    <a className="page-link" tabIndex="-1" href="false">{"<-"}</a>
                </li>

                <li className="page-item"><a className="page-link" href="false">
                    <p><span className="col-form-label">{moment(flightsInfo.arrivalDate).format("ddd, ll")}</span></p>
                    <p><label className="col-form-label">Desde</label></p>
                    <p><label className="col-form-label"><b>
                        {searchResponse.currency} {Math.min(...flightsInfo.map(info => info.lowestPrice))}
                    </b></label></p>
                </a></li>

                <li className="page-item">
                    <a className="page-link" href="false">{"->"}</a>
                </li>
            </ul></nav>

            {flightsInfo.map( (info, i) =>
            <div className="list-group flights-info" key={info.id + i}>

            <div className="list-group-item list-group-item-action flex-column align-items-start"
                onClick={() => this.collapseFlightResult(i)}>
                <div className="d-flex w-100 justify-content-between">
                    <h6 className="mb-1">
                        Salida ({info.from.name}) {moment(info.departureDate).format('LT')} -
                        Llegada ({info.to.name}) {moment(info.arrivalDate).format('LT')} &nbsp; &nbsp;
                    </h6>
                    <small>
                        {info.flightNumber}
                    </small>
                </div>
                <small className="float-left">
                    Desde {searchResponse.currency} {info.lowestPrice}
                </small>
                <small className="float-right">
                    {info.segments.length > 1
                    ? "Vuelo con escalas"
                    : ""}
                </small>
            </div>

            <Collapse isOpened={this.state.isOpenedId === i ? true : false}>

                <div className="input-group input-group-lg">
                    {info.fares.map( faresInfo =>
                    <Link to={{
                        pathname: '/asientos',
                        state: {
                            createTripData: this.createTripData(
                                info.key, faresInfo.availabilityKey, faresInfo.price
                            )
                        }
                    }} key={faresInfo.id} >
                        <div className="card" style={{width: "18rem"}}>
                        <div className="card-body">
                            <h5 className="card-title">{faresInfo.name}</h5>
                            <p><small className="card-text"><b>Price:</b> {faresInfo.price}</small></p>
                            <p><small className="card-text"><b>Viva club price:</b> {faresInfo.vivaClubPrice}</small></p>
                            <p><small className="card-text"><b>Price without discount:</b> {faresInfo.priceWithoutDiscount}</small></p>
                        </div>
                        </div>
                    </Link>
                    )}
                </div>

            </Collapse>
            </div>
            )}

            </div>
        }</div>
        );
    }
}

const mapStateToProps = state => {
  return {
    searchResponse: get(state, 'flightsData.searchResponse', [])
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ ...flightsDataActions }, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FlightForm);
