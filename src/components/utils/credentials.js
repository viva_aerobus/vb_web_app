export default function createHeaders(x_token){
    const apiHeaders = {
        "x-useridentifier": "q3TSS2lKKC7tYQd8VNf3J6AEBvVhcs",
        "x-token": x_token,
        "x-externalrateid": "J6JYKMJUEV7FU6M",
        "authority": "api-test-vivaair.ezyflight.se",
        "x-bookingcurrencycode": "USD",
    }
    return apiHeaders
}