import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import get from 'lodash/get';
import DatePicker from 'react-datepicker';
import * as formDataActions from '../vb_data_layer/actions/formData';

class IndexForm extends Component {
    state = {
        tripType: 'oneway',
        startDate: null,
        endDate: null,
        origin: '',
        destination: null,
        passengers: [
        {
            count: 1,
            code: 'ADT'
        },
        {
            count: 0,
            code: 'CHD'
        },
        {
            count: 0,
            code: 'INFT'
        }
        ],
        currencyCode: 'COP'
    };

    async componentDidMount() {
        this.props.actions.getFormData();
    }

    toggleChange(value) {
        this.setState({ tripType: value });
    }

    passChange(code, value, mode) {
        let passengers = [...this.state.passengers];
        mode === "minus" ? value-- : value++;
        passengers.find(pass => pass.code === code).count = value;
        this.setState({ passengers });
    }

    dateChange(value, type) {
        this.setState({ [type]: value });
    };

    selectChange(e, type) {
        this.setState({ [type]: e.target.value });
    }

    createSearchApi = () => {
        const searchFlights = {
            "passengers": this.state.passengers,
            "currencyCode": this.state.currencyCode,
            "routes": [
            {
                "fromAirport": this.state.origin,
                "startDate": moment(this.state.startDate).format('L'),
                "endDate": moment(this.state.startDate).format('L'),
                "toAirport": this.state.destination
            }],
            "isManageSearch": false
        }

        const routesReturn = {
            "fromAirport": this.state.destination,
            "startDate": moment(this.state.endDate).format('L'),
            "endDate": moment(this.state.endDate).format('L'),
            "toAirport": this.state.origin
        }

        if( this.state.tripType === "roundtrip" ){
            searchFlights.routes.push(routesReturn)
        }

        return searchFlights
    }

    render() {
        const countAdt = this.state.passengers.find(pass => pass.code === "ADT").count;
        const countChd = this.state.passengers.find(pass => pass.code === "CHD").count;
        const countInft = this.state.passengers.find(pass => pass.code === "INFT").count;
        const { airportsArray, configArray, currenciesArray } = this.props;

        return (
        <form noValidate>
        <div className="card text-center card-form">
            <div className="card-body">
            <div className="form-row form-group">
                <div className="input-group col">
                    <div className="form-check form-check-inline">
                        <input className="form-check-input" type="checkbox" id="inlineCheckbox1" value="oneway"
                            checked = {this.state.tripType === "oneway" ? true : false }
                            onChange = {() => this.toggleChange("oneway")}
                        />
                        <label className="form-check-label" >Solo ida</label>
                    </div>
                    <div className="form-check form-check-inline">
                        <input className="form-check-input" type="checkbox" id="inlineCheckbox2" value="roundtrip"
                            checked = {this.state.tripType === 'roundtrip' ? true : false }
                            onChange = {() => this.toggleChange("roundtrip")}
                        />
                        <label className="form-check-label" >Ida y Vuelta</label>
                    </div>
                </div>
                <div className="input-group col">
                    <div className="input-group-prepend">
                        <span className="input-group-text">Moneda</span>
                    </div>
                    <select className="custom-select col-sm-2" id="currenciesCode" onChange={(e) => this.selectChange(e, 'currencyCode')} required>
                        {currenciesArray.map( val =>
                            <option value={val.code} key={val.code}>{val.code}</option>
                        )}
                    </select>
                </div>
            </div>

            <div className="form-row form-group">
                <div className="col input-group">
                    <div className="input-group-prepend">
                        <span className="input-group-text">Vuelo</span>
                    </div>
                    <select className="custom-select" id="origen" onChange={(e) => this.selectChange(e, "origin")} required>
                        <option>Origen</option>
                        {airportsArray.map( val =>
                            <option value={val.code} key={val.code}>{val.name}</option>
                        )}
                    </select>
                    <select className="custom-select" id="destino" onChange={(e) => this.selectChange(e, "destination")} required>
                        <option>Destino</option>
                        {airportsArray.map( val =>
                            <option value={val.code} key={val.code}>{val.name}</option>
                        )}
                    </select>
                </div>

                <div className="col input-group">
                    <div className="input-group-prepend">
                        <span className="input-group-text">Fecha</span>
                    </div>
                    <DatePicker
                        placeholderText="Salida"
                        className="date-picker-form"
                        minDate={new Date()}
                        onChange={(e) => this.dateChange(e, 'startDate')}
                        selected={this.state.startDate}
                    />
                    {this.state.tripType === 'roundtrip'
                    ? <DatePicker
                        className="date-picker-form"
                        placeholderText="Regreso"
                        minDate={this.state.startDate}
                        onChange={(e) => this.dateChange(e, 'endDate')}
                        selected={this.state.endDate}
                    />
                    : ""}
                </div>
            </div>

            <div className="form-row form-group">
                <div className="col input-group">
                    <label className="col-form-label">Adults</label>&nbsp;
                    <div className="input-group-prepend sum-passengers"
                        onClick={countAdt > 1 ? () => this.passChange("ADT", countAdt, "minus") : null}
                    >
                        <span className="input-group-text">-</span>
                    </div>
                    <input type="text" className="form-control sum-passengers" value={countAdt} readOnly />
                    <div className="input-group-prepend sum-passengers" onClick={() => this.passChange("ADT", countAdt, "plus")}>
                        <span className="input-group-text">+</span>
                    </div>
                </div>

                <div className="col input-group">
                    <label className="col-form-label">Childs</label>&nbsp;
                    <div className="input-group-prepend sum-passengers"
                        onClick={countChd >= 1 ? () => this.passChange("CHD", countChd, "minus") : null}
                    >
                        <span className="input-group-text">-</span>
                    </div>
                    <input type="text" className="form-control sum-passengers" value={countChd} readOnly/>
                    <div className="input-group-prepend sum-passengers" onClick={() => this.passChange("CHD", countChd, "plus")}>
                        <span className="input-group-text">+</span>
                    </div>
                </div>

                <div className="col input-group">
                    <label className="col-form-label">Infants</label>&nbsp;
                    <div className="input-group-prepend sum-passengers"
                        onClick={countInft >= 1 ? () => this.passChange("INFT", countInft, "minus") : null}
                    >
                        <span className="input-group-text">-</span>
                    </div>
                    <input type="text" className="form-control sum-passengers" value={countInft} readOnly/>
                    <div className="input-group-prepend sum-passengers" onClick={() => this.passChange("INFT", countInft, "plus")}>
                        <span className="input-group-text">+</span>
                    </div>
                </div>

                <div className="col input-group">
                    <Link to={{
                        pathname: '/vuelos',
                        state: { searchFlights: this.createSearchApi() }
                    }} >
                        <button type="button" className="btn btn-success">
                            Buscar vuelos
                        </button>
                    </Link>
                </div>
            </div>
            </div>
        </div>
        </form>
        );
    }
}

const mapStateToProps = state => {
    return {
        airportsArray: get(state, 'formData.airportsArray', []),
        configArray: get(state, 'formData.configArray', []),
        currenciesArray: get(state, 'formData.currenciesArray', []),
        process: get(state, 'formData.process', null)
    };
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({ ...formDataActions }, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IndexForm);
